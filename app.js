require('dotenv').config();
const http = require('http');
var bodyParser = require("body-parser");
var express = require("express")
var { Pool } = require('pg');
const mysql = require('mysql');



function handlePostRequest(req, res, next) {
  console.log("Received post request");
  const alert = { version: req.body.version, groupKey: req.body.groupKey, truncatedAlerts: req.body.truncatedAlerts, status: req.body.status, receiver: req.body.receiver, groupLabels: req.body.groupLabels, commonlabels: req.body.commonLabels, commonAnnotations: req.body.commonAnnotations, externalURL: req.body.externalURL} 

  connection.query('INSERT INTO alert_message SET ?', alert, (err, res) => {
    if(err) throw err;
    console.log('Last insert message ID :', res.insertId);


    if (req.body.alerts) {
      req.body.alerts.forEach( element =>
        connection.query('INSERT INTO alert SET ?', getAlert(element, res.insertId), (err, res) => {
          if(err) throw err;
          console.log('Last alert ID:', res.insertId);
        })
      );
    }
  });
}

function getAlert(alert, messageId) {
  const AlertMessage = {
    status: alert.status,
    labels: alert.labels,
    annotations: alert.annotations,
    startsAt: alert.startsAt,
    endsAt: alert.endsAt,
    generatorURL: alert.generatorURL,
    alert_message_id: messageId
  }

  return AlertMessage;
}

const connection = mysql.createConnection({
  host: process.env.database_host,
  user: process.env.database_user,
  password: process.env.database_pass,
  database: process.env.database_name
});

connection.connect((err) => {
  if (err) throw err;
  console.log('Connected!');
});


var app = express()
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());



app.post("/api/alert", (req, res, next) => {
  handlePostRequest(req, res, next);
  res.json({"body": req.body})
});

app.get("/api", (req, res, next) => {
	const username = req.body.username;
    res.json({"message":"Welcome to server"})
});

// Default response for any other request
app.use(function(req, res){
    res.status(404);
    res.json({"error":"This page is not available"});
});

process.on('uncaughtException', function (error) {
   console.log(error.stack);
});

let port = process.env.PORT;
if (port == null || port == "") {
  port = 8080;
}
app.listen(port);