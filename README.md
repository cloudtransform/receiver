# Receiver webhook

This project is build for the alert manager of Prometheus.
The following components are:
- The database
- The API

## Environment variables

In order to run the application, rename the environment variable file `.env.sample` to `.env` and fill in the fields.

## Setting up local database

Creating the docker image:
This example uses the environment variables used in the sample.
````shell script
docker build -t receiver-image --build-arg db_root_pw=1234 --build-arg db_name=receiver --build-arg db_username=demo --build-arg db_pw=1234 .
````

To start the dockerized SQL server, run the following command from the db folder of this project in the terminal:
````shell script
docker run -p 3306:3306 receiver-image
````
This will run the previously created image and forward the port 3306 to the local 3306
The database however still needs to be created.

#Create and seed the database

````shell script
docker ps # This will display the CONTAINER_ID for the user to copy it.

docker exec -it ID_HERE sh # Replace the ID_HERE with the value copied from the output above.

cd sql/ # Go into the sql folder

sh seed_database.sh DB_USER_HERE DB_USER_PW_HERE DB_NAME_HERE
````

In order to access the mysql instance in the docker container,
run the following commands in the terminal:

````shell script
docker ps # This will display the CONTAINER_ID for the user to copy it.

docker exec -it ID_HERE sh # Replace the ID_HERE with the value copied from the output above.

mysql -u YOUR_USER_HERE -p YOUR_PASSWORD_HERE YOUR_DATABASE_NAME_HERE # This will log the user into the mysql instance and select the database.
````