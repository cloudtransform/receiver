for file in /sql/*.sql; do
    if [ -f "$file" ]; then
        mysql -u $1 -p$2 $3 < "$file"
    fi
done